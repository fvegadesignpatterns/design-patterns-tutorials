//
//  QuestionGroupBuilder.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 03-01-20.
//  Copyright © 2020 Freddy Miguel Vega Zárate. All rights reserved.
//

class QuestionBuilder {
    var answer = ""
    var hint = ""
    var prompt = ""
    
    func build() throws -> Question {
        guard answer.count > 0 else { throw Error.missingAnswer }
        guard prompt.count > 0 else { throw Error.missingPrompt }
        
        return Question(answer: answer, hint: hint, prompt: prompt)
    }
    
    enum Error: String, Swift.Error {
        case missingAnswer
        case missingPrompt
    }
}

class QuestionGroupBuilder {
    // 1
    var questions = [QuestionBuilder()]
    var title = ""
    
    // 2
    func addNewQuestion() {
        let question = QuestionBuilder()
        questions.append(question)
    }
    
    func removeQuestion(at index: Int) {
        questions.remove(at: index)
    }
    
    // 3
    func build() throws -> QuestionGroup {
        guard title.count > 0 else { throw Error.missingTitle }
        guard questions.count > 0 else { throw Error.missingQuestions }
        let questions = try self.questions.map { try $0.build() }
        
        return QuestionGroup(questions: questions, title: title)
    }
    
    enum Error: String, Swift.Error {
        case missingTitle
        case missingQuestions
    }
}
