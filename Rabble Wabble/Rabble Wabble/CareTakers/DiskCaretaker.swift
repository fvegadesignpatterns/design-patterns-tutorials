//
//  DiskCaretaker.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 13-12-19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import Foundation

class DiskCaretaker {
    static let decoder = JSONDecoder()
    static let encoder = JSONEncoder()
    
    static func createDocumentURL(withFileName filename: String) -> URL {
        let fileManager = FileManager.default
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        return url.appendingPathComponent(filename).appendingPathExtension("json")
    }
    
    // 1
    static func save<T: Codable>(_ object: T, to fileName: String) throws {
        do {
            // 2
            let url = createDocumentURL(withFileName: fileName)
            // 3
            let data = try encoder.encode(object)
            // 4
            try data.write(to: url, options: .atomic)
        } catch (let error) {
            // 5
            print("Save failed: Object: '\(object)', Error: '\(error)'")
            throw error
        }
    }
    
    // 6
    static func retrieve<T: Codable>(_ type: T.Type, from fileName: String) throws -> T {
        let url = createDocumentURL(withFileName: fileName)
        
        return try retrieve(T.self, from: url)
    }
    
    // 7
    static func retrieve<T: Codable>(_ type: T.Type, from url: URL) throws -> T {
        do {
            // 8
            let data = try Data(contentsOf: url)
            // 9
            return try decoder.decode(T.self, from: data)
        } catch (let error) {
            // 10
            print("Retrieve failed: URL: '\(url)', Error: '\(error)'")
            throw error
        }
    }
}
