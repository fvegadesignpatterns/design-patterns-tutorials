//
//  QuesionGroupCaretaker.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 13-12-19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import Foundation

// 1
final class QuestionGroupCaretaker {
    // MARK: - Properties
    // 2
    private let fileName = "QuestionGroupData"
    var questionGroups = [QuestionGroup]()
    var selectedQuestionGroup: QuestionGroup!
    
    // MARK: - Object Lifecycle
    init() {
        // 3
        loadQuestionGroups()
    }
    
    // 4
    func loadQuestionGroups() {
        if let questionGroups = try? DiskCaretaker.retrieve([QuestionGroup].self, from: fileName) {
            self.questionGroups = questionGroups
        } else {
            let bundle = Bundle.main
            let url = bundle.url(forResource: fileName, withExtension: "json")!
            self.questionGroups = try! DiskCaretaker.retrieve([QuestionGroup].self, from: url)
            try! save()
        }
    }
    
    func deleteQuestionGroup(at index: Int) {
        guard questionGroups.count > 0 else { return }
        questionGroups.remove(at: index)
        try? save()
    }
    
    // MARK: - Instance Methods
    // 5
    func save() throws {
        try DiskCaretaker.save(questionGroups, to: fileName)
    }
}
