//
//  SelectQuestionGroupViewController.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/29/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import UIKit

class SelectQuestionGroupViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView()
        }
    }
    
    // MARK: - Properties
    let appSettings = AppSettings.shared
    let questionGroupCaretaker = QuestionGroupCaretaker()
    var questionGroups: [QuestionGroup] {
        return questionGroupCaretaker.questionGroups
    }
    private var selectedQuestionGroup: QuestionGroup! {
        get {
            return questionGroupCaretaker.selectedQuestionGroup
        }
        set {
            questionGroupCaretaker.selectedQuestionGroup = newValue
        }
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        questionGroups.forEach {
            print("\($0.title): correctCount \($0.score.correctCount),  incorrectCount \($0.score.incorrectCount)")
        }
    }
}

// MARK: - UITableViewDataSource
extension SelectQuestionGroupViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionGroups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionGroupCell") as? QuestionGroupCell else {
            return UITableViewCell()
        }
        let questionGroup = questionGroups[indexPath.row]
        cell.titleLabel.text = questionGroup.title
        cell.percentageSuscriber = questionGroup.score.$runningPercentage.receive(on: DispatchQueue.main).map() {
            return String(format: "%.0f %%", round(100 * $0))
        }.assign(to: \.text, on: cell.percentageLabel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        let deleteAction = UIContextualAction(
            style: .destructive,
            title: "Delete"
        ) { (action, view, completionHandler) in
            let alert = UIAlertController(
                title: "Are you sure to delete group \(self.questionGroupCaretaker.questionGroups[indexPath.row].title)?",
                message: "This action cannot be undone",
                preferredStyle: .alert
            )
            let deleteAction = UIAlertAction(
                title: "Delete",
                style: .destructive
            ) { alertAction in
                self.questionGroupCaretaker.deleteQuestionGroup(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            alert.addAction(deleteAction)
            
            self.present(alert, animated: true)
            completionHandler(true)
        }
        let editAction = UIContextualAction(style: .normal, title: "Edit") { (action, view, completionHandler) in
            // TODO: Edit question groups
            print("Edit")
            completionHandler(true)
        }
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}

// MARK: - UITableViewDelegate
extension SelectQuestionGroupViewController: UITableViewDelegate {
    
    // 1
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedQuestionGroup = questionGroups[indexPath.row]
        return indexPath
    }
    
    // 2
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // 3
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // 1
        if let viewController = segue.destination as? QuestionViewController {
            viewController.questionStrategy = appSettings.questionStrategy(for: questionGroupCaretaker)
            viewController.delegate = self
            
            // 2
        } else if let navController = segue.destination as? UINavigationController, let viewController = navController.topViewController as? CreateQuestionGroupViewController {
            viewController.delegate = self
        }
        
        // 3
        // Whatevs... Skip anything else
    }
}

extension SelectQuestionGroupViewController: QuestionViewControllerDelegate {
    func questionViewController(_ viewController: QuestionViewController,
                                didCancel questionStrategy: QuestionStrategy, at questionIndex: Int) {
        navigationController?.popToViewController(self, animated: true)
    }
    
    func questionViewController(_ viewController: QuestionViewController,
                                didComplete questionStrategy: QuestionStrategy) {
        navigationController?.popToViewController(self, animated: true)
    }
}

// MARK: - CreateQuestionGroupViewControllerDelegate
extension SelectQuestionGroupViewController: CreateQuestionGroupViewControllerDelegate {
    func createQuestionGroupViewControllerDidCancel(_ viewController: CreateQuestionGroupViewController) {
        dismiss(animated: true)
    }
    
    func createQuestionGroupViewController(_ viewController: CreateQuestionGroupViewController, created questionGroup: QuestionGroup) {
        questionGroupCaretaker.questionGroups.append(questionGroup)
        try? questionGroupCaretaker.save()
        
        dismiss(animated: true)
        tableView.reloadData()
    }
}
