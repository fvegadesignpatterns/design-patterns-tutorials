//
//  Question.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/28/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import Foundation

class Question: Codable {
    let answer: String
    let hint: String?
    let prompt: String
    
    init(answer: String, hint: String?, prompt: String) {
        self.answer = answer
        self.hint = hint
        self.prompt = prompt
    }
}
