//
//  QuestionGroup.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/28/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import Foundation
import Combine

class QuestionGroup: Codable {
    class Score: Codable {
        var correctCount = 0 {
            didSet {
                updateRunningpercentage()
            }
        }
        var incorrectCount = 0 {
            didSet {
                updateRunningpercentage()
            }
        }
        @Published
        var runningPercentage: Double = 0
        
        init() {}
        
        // 1
        private enum CodingKeys: String, CodingKey {
            case correctCount
            case incorrectCount
        }
        
        // 2
        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            correctCount = try container.decode(Int.self, forKey: .correctCount)
            incorrectCount = try container.decode(Int.self, forKey: .incorrectCount)
            updateRunningpercentage()
        }
        
        // 3
        private func updateRunningpercentage() {
            let totalCount = correctCount + incorrectCount
            guard totalCount > 0 else {
                runningPercentage = 0
                return
            }
            
            runningPercentage = Double(correctCount) / Double(totalCount)
        }
        
        func reset() {
            correctCount = 0
            incorrectCount = 0
        }
    }
    
    let questions: [Question]
    private(set) var score: Score
    let title: String
    
    init(questions: [Question], score: Score = Score(), title: String) {
        self.questions = questions
        self.score = score
        self.title = title
    }
}
