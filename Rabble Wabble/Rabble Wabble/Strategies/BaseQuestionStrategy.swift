//
//  BaseQuestionStrategy.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 13-12-19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import Foundation

class BaseQuestionStrategy: QuestionStrategy {
    // MARK: - Properties
    // 1
    var correctCount: Int {
        get {
            return questionGroup.score.correctCount
        }
        set {
            questionGroup.score.correctCount = newValue
        }
    }
    var incorrectCount: Int {
        get {
            return questionGroup.score.incorrectCount
        }
        set {
            questionGroup.score.incorrectCount = newValue
        }
    }
    private var questionGroupCaretaker: QuestionGroupCaretaker
    // 2
    private var questionGroup: QuestionGroup {
        return questionGroupCaretaker.selectedQuestionGroup
    }
    private var questionIndex = 0
    private let questions: [Question]
    
    // MARK: - Object Lifecycle
    // 3
    init(questionGroupCaretaker: QuestionGroupCaretaker, questions: [Question]) {
        self.questionGroupCaretaker = questionGroupCaretaker
        self.questions = questions
        // 4
        self.questionGroupCaretaker.selectedQuestionGroup.score.reset()
    }
    
    // MARK: - QuestionStrategy
    var title: String {
        return questionGroup.title
    }
    
    func currentQuestion() -> Question {
        return questions[questionIndex]
    }
    
    func advanceToNextQuestion() -> Bool {
        try? questionGroupCaretaker.save()
        guard questionIndex + 1 < questions.count else {
            return false
        }
        questionIndex += 1
        
        return true
    }
    
    func markQuestionCorrect(_ question: Question) {
        correctCount += 1
    }
    
    func markQuestionIncorrect(_ question: Question) {
        incorrectCount += 1
    }
    
    func questionIndexTitle() -> String {
        return "\(questionIndex + 1)/\(questions.count)"
    }
}
