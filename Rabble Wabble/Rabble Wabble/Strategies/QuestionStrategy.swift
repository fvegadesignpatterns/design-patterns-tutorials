//
//  QuestionStrategy.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/30/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

protocol QuestionStrategy: class {
    // 1
    var title: String { get }
    
    // 2
    var correctCount: Int { get }
    var incorrectCount: Int { get }
    
    // 3
    func advanceToNextQuestion() -> Bool
    
    // 4
    func currentQuestion() -> Question
    
    // 5
    func markQuestionCorrect(_ question: Question)
    func markQuestionIncorrect(_ question: Question)
    
    // 6
    func questionIndexTitle() -> String
}
