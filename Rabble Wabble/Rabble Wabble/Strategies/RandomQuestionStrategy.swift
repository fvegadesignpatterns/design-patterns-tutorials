//
//  RandomQuestionStrategy.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/30/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

class RandomQuestionStrategy: BaseQuestionStrategy {
    convenience init(questionGroupCaretaker: QuestionGroupCaretaker) {
        let questionGroup = questionGroupCaretaker.selectedQuestionGroup!
        let questions = questionGroup.questions.shuffled()
        self.init(questionGroupCaretaker: questionGroupCaretaker, questions: questions)
    }
}
