//
//  QuestionGroupCell.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/29/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import UIKit
import Combine

class QuestionGroupCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var percentageLabel: UILabel!
    var percentageSuscriber: AnyCancellable?
}
