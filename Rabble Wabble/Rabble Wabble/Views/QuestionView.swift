//
//  QuestionView.swift
//  Rabble Wabble
//
//  Created by Freddy Miguel Vega Zárate on 10/29/19.
//  Copyright © 2019 Freddy Miguel Vega Zárate. All rights reserved.
//

import UIKit

class QuestionView: UIView {
    @IBOutlet var answerLabel: UILabel!
    @IBOutlet var correctCountLabel: UILabel!
    @IBOutlet var incorrectCountLabel: UILabel!
    @IBOutlet var promptLabel: UILabel!
    @IBOutlet var hintLabel: UILabel!
}
